var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var connectionInfo = require('./connection');
var uri = connectionInfo.uri;
const MongoClient = require('mongodb').MongoClient;

var connectedDevices = [];

function ConnectionInfo(connectionId, groupId) {
  this.connectionId = connectionId;
  this.groupId = groupId;
}

console.log('[' + new Date().toUTCString() + '] '+"index.js accessed");

app.get('/control', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.post('/addLight', function(req, res){
  console.log(req.params);
  res.sendFile(__dirname + '/admin.html');
});

io.on('connection', function(socket){
    console.log('user connected ',socket.id);
    socket.emit("sendSerialNumber","");

    socket.on('deviceInfo', function(msg){
      console.log(this.id,msg.serialNumber);
      getGroupFromSerial(parseInt(msg.serialNumber)).then(function(result){
        connectedDevices[parseInt(msg.serialNumber)] = result[0].group;
        console.log(result);
        socket.join(result[0].group);
      });
    });

    socket.on('rainbow', function(msg){
      console.log(msg);
      console.log('rainbow recieved from ' + msg.serialNumber);
      //getGroupFromSerial(parseInt(msg.serialNumber)).then(function(result){
        groupId = connectedDevices[parseInt(msg.serialNumber)];
        socket.in(groupId).emit('rainbow', msg);
      //});
    });    
    
    socket.on('plainString', function(msg){
      console.log('plainString recieved');
    });
    
    socket.on('rgb', function(data){
      currentColor = data;
      //getGroupFromSerial(parseInt(data.serialNumber)).then(function(result){
        groupId = connectedDevices[parseInt(data.serialNumber)];
        socket.in(groupId).emit("rgb",data);
      //});
      console.log(data);
    })
    
    socket.on('disconnect', function(){
      console.log('user disconnected');
    });
});

const port = process.env.PORT || 8080;

http.listen(port, function(){
  console.log('listening on *:'+port);
});

function getGroupFromSerial(serialNumber){
  return new Promise(function(resolve,reject){
      MongoClient.connect(uri, function(err, db) {
          if (err) {
              reject(err);  
          } 
          else {
              resolve(db);
          } 
      })
  }).then( function(db){      
      var dbo = db.db("mydb");
      var query = { serial: serialNumber };
      return new Promise(function(resolve,reject){
          dbo.collection("serialNumbers").find(query).toArray(function(err, result) {
              if (err) {
                  db.close();
                  reject(err)
              }
              db.close();
              resolve(result);
          });
      })
  })
}

function addSerialNumber(serialNumber){
  return new Promise(function(resolve,reject){
      MongoClient.connect(uri, function(err, db) {
          if (err) {
              reject(err);  
          } 
          else {
              resolve(db);
          } 
      })
  }).then( function(db){      
      var dbo = db.db("mydb");
      var query = { serial: serialNumber };
      return new Promise(function(resolve,reject){
          dbo.collection("serialNumbers").find(query).toArray(function(err, result) {
              if (err) {
                  db.close();
                  reject(err)
              }
              db.close();
              resolve(result);
          });
      })
  })
}