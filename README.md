These are cloud-based, wifi-enabled, multicolor desk lamps. 

These are intended to work in groups. When a user changes the color of one light, the color of the others change to match it, no matter where they are in the world.

A way to keep in touch with relatives, friends, significant others, when geographically far away.

Each lamp has a ESP8266 Wifi Module that communicates through a WebSocket to a NodeJS server.
